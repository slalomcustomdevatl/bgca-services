﻿using BGCAServices.Data;
using BGCAServices.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BGCAServices.WebControllers
{
    public class ActivityController : Controller
    {

        private BGCAEntities db = new BGCAEntities();

        //
        // GET: /Activity/ApproveActivity/{guid: id}
        public ActionResult ApproveActivity(Guid id)
        {
            Activity activity = db.Activities.SingleOrDefault(w => w.approvalid == id);
            if(activity != null && activity.isapproved == false) {
                activity.isapproved = true;
                db.Entry(activity).State = System.Data.Entity.EntityState.Modified;
                try
                {
                    db.SaveChanges();
                    ViewBag.Message = "Activity has been approved!";
                }
                catch (DbUpdateConcurrencyException)
                {
                    ViewBag.Message = "Unable to approve activity at this time.  Please try again later.";
                }
            }
            else
            {
                ViewBag.Message = "Activity has already been approved or approval code is invalid.";
            }
            return View("ApproveActivity");
        }

        //
        // GET: /Activity/RejectActivity/{guid: id}
        public ActionResult RejectActivity(Guid id)
        {
            Activity activity = db.Activities.SingleOrDefault(w => w.approvalid == id);
            if (activity != null && activity.isapproved == false)
            {
                activity.isapproved = true;
                db.Entry(activity).State = System.Data.Entity.EntityState.Modified;
                try
                {
                    ActivityDTO dto = new ActivityDTO
                    {
                        id = activity.id,
                        cost = new CostDTO()
                        {
                            id = activity.Cost.id,
                            description = activity.Cost.description,
                            name = activity.Cost.name
                        },
                        name = activity.name,
                        description = activity.description,
                        isactive = activity.isactive,
                        imageurl = activity.imageurl,
                        likes = activity.likes,
                        minage = activity.minage,
                        maxage = activity.maxage,
                        gender = activity.gender,
                        activitydate = activity.activitydate,
                        activitydateend = activity.activitydateend,
                        address1 = activity.address1,
                        address2 = activity.address2,
                        city = activity.city,
                        state = activity.state,
                        zip = activity.zip,
                        categories = activity.ActivityCategories.Select(c => new CategoryDTO { id = c.Category.id, description = c.Category.description }).ToList<CategoryDTO>()
                    };
                    Rejection reject = new Rejection();
                    reject.type = "Activity";
                    reject.blob = JsonConvert.SerializeObject(dto);
                    db.Rejections.Add(reject);
                    db.Activities.Remove(activity);
                    db.SaveChanges();
                    ViewBag.Message = "Activity has been rejected!";
                }
                catch (DbUpdateConcurrencyException)
                {
                    ViewBag.Message = "Unable to reject activity at this time.  Please try again later.";
                }
            }
            else
            {
                ViewBag.Message = "Activity has already been rejected or rejection code is invalid.";
            }
            return View("RejectActivity");
        }
    }
}
