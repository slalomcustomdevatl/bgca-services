﻿using BGCAServices.Data;
using BGCAServices.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BGCAServices.WebControllers
{
    public class StoryController : Controller
    {

        private BGCAEntities db = new BGCAEntities();

        //
        // GET: /Activity/ApproveStory/{guid: id}
        public ActionResult ApproveStory(Guid id)
        {
            Story story = db.Stories.SingleOrDefault(w => w.approvalid == id);
            if (story != null && story.isapproved == false)
            {
                story.isapproved = true;
                db.Entry(story).State = System.Data.Entity.EntityState.Modified;
                try
                {
                    db.SaveChanges();
                    ViewBag.Message = "Story has been approved!";
                }
                catch (DbUpdateConcurrencyException)
                {
                    ViewBag.Message = "Unable to approve story at this time.  Please try again later.";
                }
            }
            else
            {
                ViewBag.Message = "Story has already been approved or approval code is invalid.";
            }
            return View("ApproveStory");
        }

        //
        // GET: /Activity/RejectStory/{guid: id}
        public ActionResult RejectStory(Guid id)
        {
            Story story = db.Stories.SingleOrDefault(w => w.approvalid == id);
            if (story != null && story.isapproved == false)
            {
                try
                {
                    StoryDTO dto = new StoryDTO
                    {
                        id = story.id,
                        genre = new GenreDTO { id = story.Genre.id, name = story.Genre.name },
                        ethnicity = new EthnicityDTO { id = story.Ethnicity.id, name = story.Ethnicity.name },
                        name = story.name,
                        description = story.description,
                        isfeatured = story.isfeatured,
                        isactive = story.isactive,
                        imageurl = story.imageurl,
                        videourl = story.videourl,
                        gender = story.gender,
                        age = story.age
                    };

                    Rejection reject = new Rejection();
                    reject.type = "Story";
                    reject.blob = JsonConvert.SerializeObject(dto);
                    db.Rejections.Add(reject);
                    db.Stories.Remove(story);
                    db.SaveChanges();
                    ViewBag.Message = "Story has been rejected!";
                }
                catch (DbUpdateConcurrencyException)
                {
                    ViewBag.Message = "Unable to reject story at this time.  Please try again later.";
                }
            }
            else
            {
                ViewBag.Message = "Story has already been rejected or rejection code is invalid.";
            }
            return View("RejectStory");
        }

    }
}
