﻿using BGCAServices.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BGCAServices.CodeTemplates.EmailTemplates
{
    public partial class ActivityTemplate
    {
        private ActivityDTO _activity;
        private Guid _approvalId;
        private string _genderDescriptor;
        private string _categoryDescriptor;

        public ActivityTemplate(ActivityDTO activity, Guid approvalId) {
            _activity = activity;
            _approvalId = approvalId;

            if (_activity.gender == null || _activity.gender.ToLower() == "both" || _activity.gender.ToLower() == "all")
                _genderDescriptor = "Boys and Girls";
            else if (_activity.gender.ToLower() == "male")
                _genderDescriptor = "Boys Only";
            else if (_activity.gender.ToLower() == "female")
                _genderDescriptor = "Girls Only";
            else
                _genderDescriptor = "";

            if (activity.categories != null)
            {
                foreach (CategoryDTO oneCat in activity.categories)
                {
                    _categoryDescriptor = _categoryDescriptor + oneCat.description + ", ";
                }
                _categoryDescriptor = _categoryDescriptor.Substring(0, _categoryDescriptor.Length - 2);
            }
        
        }
    }
}