﻿using BGCAServices.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BGCAServices.CodeTemplates.EmailTemplates
{
    public partial class StoryTemplate
    {
        private StoryDTO _story;
        private Guid _approvalId;
        private string _genderDescriptor;

        public StoryTemplate(StoryDTO story, Guid approvalId) {
            _story = story;
            _approvalId = approvalId;

            if (_story.gender == null || _story.gender.ToLower() == "both" || _story.gender.ToLower() == "all")
                _genderDescriptor = "Boys and Girls";
            else if (_story.gender.ToLower() == "male")
                _genderDescriptor = "Boys Only";
            else if (_story.gender.ToLower() == "female")
                _genderDescriptor = "Girls Only";
            else
                _genderDescriptor = "";

        }
    }
}