﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Amazon;
using BGCAServices.CodeTemplates;
using BGCAServices.Data;
using BGCAServices.Models;
using Amazon.SimpleEmail.Model;
using Amazon.SimpleEmail;
using Amazon.Runtime;
using Newtonsoft.Json;
using System.Configuration;

namespace BGCAServices.Classes
{
    /// <summary>
    /// Helper functions
    /// </summary>
    public class Utils
    {
        // Default image url used for empty imageUrl submissions.
        public const string _defaultImageUrl = @"https://s3.amazonaws.com/bgca-slalom/bgca-logo-black.png";


        /// <summary>
        /// Format and send a notification email relating to a Story Submission
        /// </summary>
        /// <param name="story"></param>
        /// <param name="approvalId"></param>
        public static void SendStoryNotificationEmail(StoryDTO story, Guid approvalId)
        {
            string SUBJECT = "Story Submission Requires Approval";
            // Fill in the email template with the provided Story and ApprovalId
            BGCAServices.CodeTemplates.EmailTemplates.StoryTemplate storyMessage = new BGCAServices.CodeTemplates.EmailTemplates.StoryTemplate(story, approvalId);

            string pageContent = storyMessage.TransformText();
            string BODY = pageContent.ToString();
            SendNotificationEmail(SUBJECT, BODY);
        }

        /// <summary>
        /// Format and send an email relating to an Activity Submission
        /// </summary>
        /// <param name="activity"></param>
        /// <param name="approvalId"></param>
        public static void SendActivityNotificationEmail(ActivityDTO activity, Guid approvalId)
        {
            string SUBJECT = "Activity Submission Requires Approval";
            // Fill in the email template with the provided Story and ApprovalId
            BGCAServices.CodeTemplates.EmailTemplates.ActivityTemplate activityMessage = new BGCAServices.CodeTemplates.EmailTemplates.ActivityTemplate(activity, approvalId);

            string pageContent = activityMessage.TransformText();
            string BODY = pageContent.ToString();
            SendNotificationEmail(SUBJECT, BODY);
        }

        /// <summary>
        /// Send an email with text subject and HTML body
        /// </summary>
        /// <param name="SUBJECT">The email subject</param>
        /// <param name="BODY">The email body, expected to be an HTML string</param>
        public static void SendNotificationEmail(string SUBJECT, string BODY){
            
            String FROM = "BGCAApprovals@gmail.com";  // Replace with your "From" address. This address must be verified.
            
            BGCAEntities db = new BGCAEntities();
            List<string> TO = db.Approvers.Select(o => o.email).ToList<string>();
            //List<string> TO = db.Approvers.Where(w => w.email == "platypusavenger@gmail.com").Select(o => o.email).ToList<string>();

            // Construct an object to contain the recipient address.
            Destination destination = new Destination();
            destination.ToAddresses = TO;

            // Create the subject and body of the message.
            Content subject = new Content(SUBJECT);
            Content htmlBody = new Content(BODY);
            Body body = new Body();
            body.Html = htmlBody;

            // Create a message with the specified subject and body.
            Message message = new Message(subject, body);
            
            // Assemble the email.
            SendEmailRequest request = new SendEmailRequest();
            request.Destination = destination;
            request.Message = message;
            request.Source = FROM;

            // Choose the AWS region of the Amazon SES endpoint you want to connect to. Note that your production 
            // access status, sending limits, and Amazon SES identity-related settings are specific to a given 
            // AWS region, so be sure to select an AWS region in which you set up Amazon SES. Here, we are using 
            // the US East (N. Virginia) region. Examples of other regions that Amazon SES supports are USWest2 
            // and EUWest1. For a complete list, see http://docs.aws.amazon.com/ses/latest/DeveloperGuide/regions.html 
            Amazon.RegionEndpoint REGION = Amazon.RegionEndpoint.USEast1;

            // Instantiate an Amazon SES client, which will make the service call.
            AmazonSimpleEmailServiceClient client = new AmazonSimpleEmailServiceClient(ConfigurationManager.AppSettings.Get("SESAccessKey"), ConfigurationManager.AppSettings.Get("SESSecretKey"), Amazon.RegionEndpoint.USEast1);
            
            // Send the email.
            try
            {
                Console.WriteLine("Attempting to send an email through Amazon SES by using the AWS SDK for .NET...");
                client.SendEmail(request);
                Console.WriteLine("Email sent!");
            }
            catch (Exception ex)
            {
                Console.WriteLine("The email was not sent.");
                Console.WriteLine("Error message: " + ex.Message);
            }
        }
    }
}