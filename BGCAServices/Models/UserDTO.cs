﻿using BGCAServices.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BGCAServices.Models
{
    public class UserDTO
    {
        public int id { get; set; }
        public string email { get; set; }
        public string name { get; set; }
        public List<int> activities { get; set; }
        public List<int> stories { get; set; }
    }
}