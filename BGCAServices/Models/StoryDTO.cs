﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BGCAServices.Models
{
    public class StoryDTO
    {
        public int id { get; set; }
        public GenreDTO genre { get; set; }
        public EthnicityDTO ethnicity { get; set; }
        public string name { get; set; }
        public string description { get; set; }
        public bool isfeatured { get; set; }
        public bool isactive { get; set; }
        public string imageurl { get; set; }
        public string videourl { get; set; }
        public string gender { get; set; }
        public byte? age { get; set; }
        public string submittedby { get; set; }
    }
}