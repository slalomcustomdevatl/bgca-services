﻿using BGCAServices.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BGCAServices.Models
{
    public class CostDTO
    {
        public int id { get; set; }
        public string name { get; set; }
        public string description { get; set; }

    }

}