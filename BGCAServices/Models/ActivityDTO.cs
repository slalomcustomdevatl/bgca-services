﻿using BGCAServices.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BGCAServices.Models
{
    public class ActivityDTO
    {
        public int id { get; set; }
        public CostDTO cost { get; set; }
        public string name { get; set; }
        public string description { get; set; }
        public bool isactive { get; set; }
        public int likes { get; set; }
        public string imageurl { get; set; }

        public int? minage { get; set; }
        public int? maxage { get; set; }
        public string gender { get; set; }
        public DateTime? activitydate { get; set; }
        public DateTime? activitydateend { get; set; }
        public string address1 { get; set; }
        public string address2 { get; set; }
        public string city { get; set; }
        public string state { get; set; }
        public string zip { get; set; }
        public string submittedby { get; set; }

        public double? distance { get; set; }

        public List<CategoryDTO> categories { get; set; }

    }
}