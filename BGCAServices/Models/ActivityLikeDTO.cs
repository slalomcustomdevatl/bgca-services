﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BGCAServices.Models
{
    public class ActivityLikeDTO
    {
        public int id { get; set; }
        public int activityid { get; set; }
        public int userid { get; set; }
    }
}