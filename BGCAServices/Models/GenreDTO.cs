﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BGCAServices.Models
{
    public class GenreDTO
    {
        public int id { get; set; }
        public string name { get; set; }
    }
}