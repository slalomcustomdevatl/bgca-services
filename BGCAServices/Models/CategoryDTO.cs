﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BGCAServices.Models
{
    public class CategoryDTO
    {
        public int id { get; set; }
        public string description { get; set; }
    }
}