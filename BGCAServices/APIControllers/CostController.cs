﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using BGCAServices.Data;
using BGCAServices.Models;

namespace BGCAServices.APIControllers
{
    /// <summary>
    /// Cost Controller - Basic Cost CRUD
    /// </summary>
    public class CostController : ApiController
    {
        private BGCAEntities db = new BGCAEntities();

        // GET api/Cost
        /// <summary>
        /// An iQueryable Cost lookup
        /// </summary>
        /// <returns>
        /// 200 - Success + A list of Costs
        /// 401 - Not Authorized 
        /// 500 - Internal Server Error + Exception
        /// </returns>
        public IQueryable<CostDTO> GetCosts()
        {
            return db.Costs.Select(o => new CostDTO {
                id = o.id, 
                name = o.name, 
                description = o.description
            });
        }

        // GET api/Cost/5
        /// <summary>
        /// Method Not Allowed.
        /// </summary>
        /// <param name="id">The CostId of the Cost to return.</param>
        /// <returns>
        /// 405 - Method Not Allowed
        /// </returns>
        [ResponseType(typeof(Cost))]
        public IHttpActionResult GetUser(string id)
        {
            return StatusCode(HttpStatusCode.MethodNotAllowed);
        }

        // PUT api/Cost/5
        /// <summary>
        /// Method Not Allowed.
        /// </summary>
        /// <param name="id">The CostId of the Cost to save.</param>
        /// <param name="inventoryModel">The model of the edited Cost</param>
        /// <returns>
        /// 405 - Method Not Allowed
        /// </returns>
        public IHttpActionResult Put(int id, Cost costModel)
        {
            return StatusCode(HttpStatusCode.MethodNotAllowed);
        }


        // POST api/Cost
        /// <summary>
        /// Method Not Allowed.
        /// </summary>
        /// <param name="activity">The new Cost</param>
        /// <returns>
        /// 405 - Method Not Allowed
        /// </returns>
        [ResponseType(typeof(Cost))]
        public IHttpActionResult PostUser(Cost user)
        {
            return StatusCode(HttpStatusCode.MethodNotAllowed);
        }

        // DELETE api/Cost/5
        /// <summary>
        /// Method Not Allowed.
        /// </summary>
        /// <param name="id">The CostId of the Cost to delete.</param>
        /// <returns>
        /// 405 - Method Not Allowed
        /// </returns>
        [ResponseType(typeof(Cost))]
        public IHttpActionResult Delete(int id)
        {
            return StatusCode(HttpStatusCode.MethodNotAllowed);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

    }
}