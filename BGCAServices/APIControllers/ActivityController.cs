﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using BGCAServices.Data;
using BGCAServices.Models;
using BGCAServices.Classes;
using BGCAServices.Models.Google;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Data.Entity.Spatial;

namespace BGCAServices.APIControllers
{
    /// <summary>
    /// Activity Controller - Basic CRUD for Activity
    /// </summary>
    public class ActivityController : ApiController
    {
	
		private BGCAEntities db = new BGCAEntities();

        // GET api/Activity/
        /// <summary>
        /// An iQueryable Activity lookup
        /// </summary>
        /// <param name="bygeo">optional - boolean - signifies if doing a geo search</param>
        /// <param name="lat">optional - double - latitude to be used in a geo search</param>
        /// <param name="lng">optional - double - longitude to be used in a geo search</param>
        /// <returns>
        /// 200 - Success + A list of Activity
        /// 401 - Not Authorized 
        /// 500 - Internal Server Error + Exception
        /// </returns>
        public IQueryable<ActivityDTO> Get(bool bygeo = false, double lat = 0.0, double lng = 0.0)
        {

            DbGeography location = null;

            if (bygeo) {
                location = DbGeography.FromText(string.Format("POINT ({0} {1})", lng, lat));
            }

            return db.Activities
                .Where(w => w.isactive == true && w.isapproved == true && (w.activitydate == null || w.activitydate >= DateTime.Today)
                    && (w.latlng.Distance(location) == null || w.latlng.Distance(location) <= 160000))
                    .OrderByDescending(o=>o.latlng.Distance(location).HasValue)
                    .ThenBy(o=>o.latlng.Distance(location))
                .Select(o => new ActivityDTO
            {
                id = o.id,
                cost = new CostDTO()
                {
                    id = o.Cost.id,
                    description = o.Cost.description,
                    name = o.Cost.name
                },
                name = o.name,
                description = o.description,
                isactive = o.isactive,
                imageurl = o.imageurl,
                likes = o.likes,
                minage = o.minage,
                maxage = o.maxage,
                gender = o.gender,
                activitydate = o.activitydate,
                activitydateend = o.activitydateend,
                address1 = o.address1,
                address2 = o.address2,
                distance = o.latlng.Distance(location) / 1609.34,
                city = o.city,
                state = o.state,
                zip = o.zip,
                submittedby = o.submittedby,
                categories = o.ActivityCategories.Select(c => new CategoryDTO { id = c.Category.id, description = c.Category.description}).ToList<CategoryDTO>()
            });
        }

        // GET api/Activity/Random
        /// <summary>
        /// Retrieve a Random set of Activities from the database.
        /// </summary>
        /// <returns>
        /// 200 - Success + The requested Activity.
        /// 401 - Not Authorized 
        /// 404 - Not Found + Reason
        /// </returns>
        /// [HttpGet]
        [Route("api/Activity/Random")]
		public IQueryable<ActivityDTO> GetRandom()
        {
            return Get().OrderBy(o => Guid.NewGuid());
        }

        // GET api/Activity/5
        /// <summary>
        /// Retrieve a single Activity from the database.
        /// </summary>
        /// <param name="id">The ActivityId of the Activity to return.</param>
        /// <returns>
        /// 200 - Success + The requested Activity.
        /// 401 - Not Authorized 
        /// 404 - Not Found + Reason
        /// </returns>
        [ResponseType(typeof(ActivityDTO))]
        public IHttpActionResult Get(int id)
        {
            ActivityDTO activity = Get().FirstOrDefault<ActivityDTO>(o => o.id == id);
            if (activity == null)
            {
                return this.NotFound("Activity not found.");
            }

            return Ok(activity);
        }

        // PUT api/Activity/5
        /// <summary>
        /// Method Not Allowed.
        /// </summary>
        /// <param name="id">The ActivityId of the Activity to save.</param>
        /// <param name="inventoryModel">The model of the edited Inventory</param>
        /// <returns>
        /// 405 - Method Not Allowed
        /// </returns>
        public IHttpActionResult Put(int id, ActivityDTO activityModel)
        {
            return StatusCode(HttpStatusCode.MethodNotAllowed);
        }

        // POST api/Activity/
        /// <summary>
        /// A new Activity to be added.
        /// </summary>
        /// <param name="activityDTO">The new Activity</param>
        /// <returns>
        /// 201 - Created + The new Activity
        /// 400 - Bad Request + (Invalid Model State)
        /// 401 - Not Authorized 
        /// 404 - Not Found + Reason
        /// 500 - Internal Server Error + Exception
        /// </returns>
		[ResponseType(typeof(ActivityDTO))]
		public IHttpActionResult Post(ActivityDTO activityDTO)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            try
            {
				Activity activity = new Activity();
                activity.costid = activityDTO.cost.id;
                activity.name = activityDTO.name;
                activity.description = activityDTO.description;
                activity.isactive = activityDTO.isactive;
                activity.imageurl = activityDTO.imageurl;
                if (activityDTO.imageurl != null)
                    activity.imageurl = activityDTO.imageurl;
                else
                    activity.imageurl = Classes.Utils._defaultImageUrl;
                activity.likes = activityDTO.likes;
                activity.minage = activityDTO.minage;
                activity.maxage = activityDTO.maxage;
                activity.gender = activityDTO.gender;
                activity.activitydate = activityDTO.activitydate;
                activity.activitydateend = activityDTO.activitydateend;
                activity.address1 = activityDTO.address1;
                activity.address2 = activityDTO.address2;
                activity.city = activityDTO.city;
                activity.state = activityDTO.state;
                activity.zip = activityDTO.zip;
                activity.approvalid = Guid.NewGuid();
                activity.submittedby = activityDTO.submittedby;

                db.Activities.Add(activity);
                db.SaveChanges();

                if (activityDTO.categories != null)
                {
                    foreach (CategoryDTO oneCategory in activityDTO.categories)
                    {
                        ActivityCategory newCategory = new ActivityCategory();
                        newCategory.activityid = activity.id;
                        newCategory.categoryid = oneCategory.id;
                        db.ActivityCategories.Add(newCategory);
                    }
                    db.SaveChanges();
                }

                activityDTO.id = activity.id;

                // Fire off an email asynchronously
                new Task(() => { Utils.SendActivityNotificationEmail(activityDTO, activity.approvalid); }).Start();

                // Fire off a task to attempt to geocode the address
                if((activity.address1 != null && activity.city != null && activity.state != null) || activity.zip != null)
                {
                    new Task(() => GeocodeAddressForActivity(activity)).Start();
                }

                return CreatedAtRoute("DefaultApi", new { id = activity.id }, activityDTO);
            }
            catch (APIException ex)
            {
                if (ex.code == 404)
                    return this.NotFound(ex.message);
                else
                    return this.InternalServerError(ex);
            }
            catch (Exception e)
            {
                return this.InternalServerError(e);
            }
        }

        // DELETE api/Activity/5
        /// <summary>
        /// Delete a Activity from the database.
        /// </summary>
        /// <param name="id">The ActivityId of the Activity to delete.</param>
        /// <returns>
        /// 405 - Method Not Allowed
        /// </returns>
		[ResponseType(typeof(ActivityDTO))]
		public IHttpActionResult Delete(int id)
        {
            return StatusCode(HttpStatusCode.MethodNotAllowed);  
        }

        private void GeocodeAddressForActivity(Activity activity)
        {
            string url = String.Format("https://maps.googleapis.com/maps/api/geocode/json?address={0} {1} {2} {3}", activity.address1, activity.city, activity.state, activity.zip);
            var json = JsonConvert.DeserializeObject <GeocodeReturn>(new WebClient().DownloadString(url));
            if (json.results != null && json.results.Count > 0)
            {
                var latlng = json.results[0].geometry.location;
                activity.latlng = DbGeography.FromText(string.Format("POINT ({0} {1})", latlng.lng, latlng.lat));
                db.Entry(activity).State = EntityState.Modified;
                db.SaveChanges();
            }
        }
    }
}
