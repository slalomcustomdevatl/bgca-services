﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using BGCAServices.Data;
using BGCAServices.Models;
using BGCAServices.Classes;

namespace BGCAServices.APIControllers
{
    /// <summary>
    /// Genre Controller - Basic CRUD for Genre
    /// </summary>
    public class GenreController : ApiController
    {
	
		private BGCAEntities db = new BGCAEntities();

        // GET api/Genre/
        /// <summary>
        /// An iQueryable Genre lookup
        /// </summary>
        /// <returns>
        /// 200 - Success + A list of Genre
        /// 401 - Not Authorized 
        /// 500 - Internal Server Error + Exception
        /// </returns>
        public IQueryable<GenreDTO> Get()
        {
            return db.Genres.Select(o => new GenreDTO { id = o.id, name = o.name });

        }

        // GET api/Genre/5
        /// <summary>
        /// Method Not Allowed
        /// </summary>
        /// <param name="id">The GenreId of the Genre to return.</param>
        /// <returns>
        /// 405 - Method Not Allowed
        /// </returns>
		[ResponseType(typeof(GenreDTO))]
		public IHttpActionResult Get(int id)
        {
            return StatusCode(HttpStatusCode.MethodNotAllowed);
        }

        // PUT api/Genre/5
        /// <summary>
        /// Method Not Allowed
        /// </summary>
        /// <param name="id">The GenreId of the Genre to save.</param>
        /// <param name="Genre">The DTO of the edited Genre</param>
        /// <returns>
        /// 405 - Method Not Allowed
        /// </returns>
        public IHttpActionResult Put(int id, GenreDTO Genre)
        {
            return StatusCode(HttpStatusCode.MethodNotAllowed);
        }

        // POST api/Genre/
        /// <summary>
        /// Method Not Allowed
        /// </summary>
        /// <param name="Genre">The new Genre</param>
        /// <returns>
        /// 405 - Method Not Allowed
        /// </returns>
		[ResponseType(typeof(Genre))]
		public IHttpActionResult Post(GenreDTO Genre)
        {
            return StatusCode(HttpStatusCode.MethodNotAllowed);
        }

        // DELETE api/Genre/5
        /// <summary>
        /// Method Not Allowed
        /// </summary>
        /// <param name="id">The GenreId of the Genre to delete.</param>
        /// <returns>
        /// 405 - Method Not Allowed
        /// </returns>
		[ResponseType(typeof(GenreDTO))]
		public IHttpActionResult Delete(int id)
        {
            return StatusCode(HttpStatusCode.MethodNotAllowed);
        }
    }
}
