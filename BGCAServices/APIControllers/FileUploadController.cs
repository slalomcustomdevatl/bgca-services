﻿using Amazon.S3;
using Amazon.S3.Model;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using ImageResizer;

namespace BGCAServices.APIControllers
{
    /// <summary>
    /// A controller providing S3 File Uploads with returned URLs
    /// </summary>
    public class FileUploadController : ApiController
    {
        private static IAmazonS3 client;

        /// <summary>
        /// Uploads a file.  Only the first file in the request will be processed.
        /// </summary>
        /// <param name="crop">List of coordinates to crop the image to "x1,y1,x2,y2".  Only checked for images.  (e.g. "0,0,200,200" yields the top left 200x200 square) </param>
        /// <returns>
        /// 201 - Created + the URL of the newly uploaded file
        /// 204 - No Content if there were no files attached
        /// 500 - Internal Server Error + error 
        /// </returns>
        [ResponseType(typeof(string))]
        [HttpPost]
        public async Task<HttpResponseMessage> PostAttachment()
        {
            return await PostAttachment("crop='max'");
        }

        /// <summary>
        /// Uploads a file.  Only the first file in the request will be processed.
        /// </summary>
        /// <param name="crop">List of coordinates to crop the image to "x1,y1,x2,y2".  Only checked for images.  (e.g. "0,0,200,200" yields the top left 200x200 square) </param>
        /// <returns>
        /// 201 - Created + the URL of the newly uploaded file
        /// 204 - No Content if there were no files attached
        /// 500 - Internal Server Error + error 
        /// </returns>
        [ResponseType(typeof(string))]
        [HttpPost]
        public async Task<HttpResponseMessage> PostAttachment(string crop)
        {
            try
            {
                HttpRequestMessage request = this.Request;
                string root = System.Web.HttpContext.Current.Server.MapPath("~/App_Data/uploads");
                var provider = new MultipartFormDataStreamProvider(root);

                if (!Request.Content.IsMimeMultipartContent())
                {
                    throw new HttpResponseException(HttpStatusCode.UnsupportedMediaType);
                }

                var task = await request.Content.ReadAsMultipartAsync(provider);
                if (provider.FileData.Count > 0)
                {
                    FileInfo finfo = new FileInfo(provider.FileData.First().LocalFileName);
                    string mimetype = provider.FileData.First().Headers.ContentType.MediaType;

                    if (crop != null && mimetype.ToLower().Contains("image/"))
                    {
                        ImageBuilder.Current.Build(finfo.FullName, finfo.FullName, new ResizeSettings("crop=" + crop));
                        //ImageBuilder.Current.Build(finfo.FullName, finfo.FullName + "_thumb", new ResizeSettings("width=200&height=200&crop=max"));
                    }

                    using (client = new AmazonS3Client(ConfigurationManager.AppSettings.Get("S3AccessKey"), ConfigurationManager.AppSettings.Get("S3SecretKey"), Amazon.RegionEndpoint.USEast1))
                    {

                        if (ModelState.IsValid)
                        {
                            string url = ConfigurationManager.AppSettings.Get("S3BucketPath") + "uploads/";
                            string filename = Guid.NewGuid().ToString();
                            
                            try
                            {
                                PutObjectRequest por = new PutObjectRequest
                                {
                                    BucketName = ConfigurationManager.AppSettings.Get("S3BucketName"),
                                    Key = "uploads/" + filename,
                                    CannedACL = S3CannedACL.PublicRead,     // TODO: Figure out a better ACL
                                    ContentType = mimetype,
                                    FilePath = finfo.FullName
                                };
                                PutObjectResponse response = client.PutObject(por);
                            }
                            catch (AmazonS3Exception amazonS3Exception)
                            {
                                return Request.CreateResponse(HttpStatusCode.InternalServerError, amazonS3Exception);
                            }

                            //Clean up files
                            finfo.Delete();
                            
                            return Request.CreateResponse(HttpStatusCode.Created, url + filename);
                        }
                        else
                        {
                            return Request.CreateResponse(HttpStatusCode.BadRequest, "Invalid Model State");
                        }
                    }
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.NoContent);
                }
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ex);
            }
        }
    }
}
