﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using BGCAServices.Data;
using BGCAServices.Models;
using BGCAServices.Classes;
using System.Threading.Tasks;

namespace BGCAServices.APIControllers
{
    /// <summary>
    /// Story Controller - Basic CRUD for Story
    /// </summary>
    public class StoryController : ApiController
    {
	
		private BGCAEntities db = new BGCAEntities();

        // GET api/Story/
        /// <summary>
        /// An iQueryable Story lookup
        /// </summary>
        /// <returns>
        /// 200 - Success + A list of Story
        /// 401 - Not Authorized 
        /// 500 - Internal Server Error + Exception
        /// </returns>
        public IQueryable<StoryDTO> Get()
        {
            return db.Stories
                .Where(w => w.isactive == true && w.isapproved == true)
                .Select(o => new StoryDTO
            {
                id = o.id,
                genre = new GenreDTO { id = o.Genre.id, name = o.Genre.name },
                ethnicity = new EthnicityDTO { id = o.Ethnicity.id, name = o.Ethnicity.name },
                name = o.name, 
                description = o.description,
                isfeatured = o.isfeatured,
                isactive = o.isactive,
                imageurl = o.imageurl,
                videourl = o.videourl,
                gender = o.gender,
                age = o.age,
                submittedby = o.submittedby
            });
        }

        // GET api/Story/5
        /// <summary>
        /// Retrieve a single Story from the database.
        /// </summary>
        /// <param name="id">The StoryId of the Story to return.</param>
        /// <returns>
        /// 200 - Success + The requested Story.
        /// 401 - Not Authorized 
        /// 404 - Not Found + Reason
        /// </returns>
		[ResponseType(typeof(StoryDTO))]
		public IHttpActionResult Get(int id)
        {
            StoryDTO story = Get().FirstOrDefault<StoryDTO>(o => o.id == id);
            if (story == null)
            {
                return this.NotFound("Story not found.");
            }

            return Ok(story);
        }

        // PUT api/Story/5
        /// <summary>
        /// Method Not Allowed.
        /// </summary>
        /// <param name="id">The StoryId of the Story to save.</param>
        /// <param name="storyDTO">The DTO of the edited Story</param>
        /// <returns>
        /// 405 - Method Not Allowed.
        /// </returns>
        public IHttpActionResult Put(int id, StoryDTO storyDTO)
        {
            return StatusCode(HttpStatusCode.MethodNotAllowed);
        }

        // POST api/Story/
        /// <summary>
        /// A new Story to be added.
        /// </summary>
        /// <param name="storyDTO">The new Story</param>
        /// <returns>
        /// 201 - Created + The new Story
        /// 400 - Bad Request + (Invalid Model State)
        /// 401 - Not Authorized 
        /// 404 - Not Found + Reason
        /// 500 - Internal Server Error + Exception
        /// </returns>
		[ResponseType(typeof(StoryDTO))]
		public IHttpActionResult Post(StoryDTO storyDTO)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            try
            {
				Story story = new Story();
                story.genreid = storyDTO.genre.id;
                story.ethnicityid = storyDTO.ethnicity.id;
                story.name = storyDTO.name;
                story.description = storyDTO.description;
                story.isfeatured = storyDTO.isfeatured;
                story.isactive = storyDTO.isactive;
                if (storyDTO.imageurl != null)
                    story.imageurl = storyDTO.imageurl;
                else
                    story.imageurl = Classes.Utils._defaultImageUrl;
                story.videourl = storyDTO.videourl;
                story.gender = storyDTO.gender;
                story.age = storyDTO.age;

                story.isapproved = false;
                story.approvalid = Guid.NewGuid();
                story.submittedby = storyDTO.submittedby;

                db.Stories.Add(story);
                db.SaveChanges();

                storyDTO.id = story.id;

                // Fire off an email asynchronously
                new Task(() => { Utils.SendStoryNotificationEmail(storyDTO, story.approvalid); }).Start();

                return CreatedAtRoute("DefaultApi", new { id = story.id }, storyDTO);
            }
            catch (APIException ex)
            {
                if (ex.code == 404)
                    return this.NotFound(ex.message);
                else
                    return this.InternalServerError(ex);
            }
            catch (Exception e)
            {
                return this.InternalServerError(e);
            }
        }

        // DELETE api/Story/5
        /// <summary>
        /// Method Not Allowed.
        /// </summary>
        /// <param name="id">The StoryId of the Story to delete.</param>
        /// <returns>
        /// 405 - Method Not Allowed
        /// </returns>
		[ResponseType(typeof(StoryDTO))]
		public IHttpActionResult Delete(int id)
        {
			return StatusCode(HttpStatusCode.MethodNotAllowed); 
        }
    }
}
