﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using BGCAServices.Data;

namespace BGCAServices.APIControllers
{
    public class OrgMetaDataController : ApiController
    {
        private BGCAEntities db = new BGCAEntities();

        // GET api/OrgMetaData
        /// <summary>
        /// An iQueryable OrgMetaData lookup
        /// </summary>
        /// <returns>
        /// 200 - Success + A list of OrgMetaDatas
        /// 401 - Not Authorized 
        /// 500 - Internal Server Error + Exception
        /// </returns>
        public IQueryable<OrgMetaData> GetOrgMetaDatas()
        {
            return db.OrgMetaDatas;
        }

        // GET api/OrgMetaData/5
        /// <summary>
        /// Gets the specific metadata for a given org.  Note this this should be queried based on the organization_id for the given clubs from cartodb.
        /// </summary>
        /// <param name="id">The OrgMetaDataId.</param>
        /// <returns>
        /// 200 - Success + The requested metadata.
        /// 401 - Not Authorized 
        /// 404 - Not Found + Reason
        /// </returns>
        [ResponseType(typeof(OrgMetaData))]
        public IHttpActionResult GetOrgMetaData(int id)
        {
            OrgMetaData orgmetadata = db.OrgMetaDatas.FirstOrDefault(f => f.organizationid == id);
            if (orgmetadata == null)
            {
                return NotFound();
            }

            return Ok(orgmetadata);
        }

        // PUT api/OrgMetaData/5
        /// <summary>
        /// PUTs not allowed yet on OrgMetaData
        /// </summary>
        public IHttpActionResult PutOrgMetaData(int id, OrgMetaData orgmetadata)
        {
            //if (!ModelState.IsValid)
            //{
            //    return BadRequest(ModelState);
            //}

            //if (id != orgmetadata.id)
            //{
            //    return BadRequest();
            //}

            //db.Entry(orgmetadata).State = EntityState.Modified;

            //try
            //{
            //    db.SaveChanges();
            //}
            //catch (DbUpdateConcurrencyException)
            //{
            //    if (!OrgMetaDataExists(id))
            //    {
            //        return NotFound();
            //    }
            //    else
            //    {
            //        throw;
            //    }
            //}

            //return StatusCode(HttpStatusCode.NoContent);
            return StatusCode(HttpStatusCode.MethodNotAllowed);
        }

        // POST api/OrgMetaData
        /// <summary>
        /// POST not allowed yet on OrgMetaData
        /// </summary>
        [ResponseType(typeof(OrgMetaData))]
        public IHttpActionResult PostOrgMetaData(OrgMetaData orgmetadata)
        {
            //if (!ModelState.IsValid)
            //{
            //    return BadRequest(ModelState);
            //}

            //db.OrgMetaDatas.Add(orgmetadata);
            //db.SaveChanges();

            //return CreatedAtRoute("DefaultApi", new { id = orgmetadata.id }, orgmetadata);

            return StatusCode(HttpStatusCode.MethodNotAllowed);
        }

        // DELETE api/OrgMetaData/5
        /// <summary>
        /// Deletes not allowed yet on OrgMetaData
        /// </summary>
        //[ResponseType(typeof(OrgMetaData))]
        //public IHttpActionResult DeleteOrgMetaData(int id)
        //{
        //    OrgMetaData orgmetadata = db.OrgMetaDatas.Find(id);
        //    if (orgmetadata == null)
        //    {
        //        return NotFound();
        //    }

        //    db.OrgMetaDatas.Remove(orgmetadata);
        //    db.SaveChanges();

        //    return Ok(orgmetadata);
        //}

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool OrgMetaDataExists(int id)
        {
            return db.OrgMetaDatas.Count(e => e.id == id) > 0;
        }
    }
}