﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using BGCAServices.Data;
using BGCAServices.Models;
using BGCAServices.Classes;

namespace BGCAServices.APIControllers
{
    /// <summary>
    /// Ethnicity Controller - Basic CRUD for Ethnicity
    /// </summary>
    public class EthnicityController : ApiController
    {
	
		private BGCAEntities db = new BGCAEntities();

        // GET api/Ethnicity/
        /// <summary>
        /// An iQueryable Ethnicity lookup
        /// </summary>
        /// <returns>
        /// 200 - Success + A list of Ethnicity
        /// 401 - Not Authorized 
        /// 500 - Internal Server Error + Exception
        /// </returns>
        public IQueryable<EthnicityDTO> Get()
        {
            return db.Ethnicities.Select(o => new EthnicityDTO {id = o.id, name = o.name });
        }

        // GET api/Ethnicity/5
        /// <summary>
        /// Method Not Allowed.
        /// </summary>
        /// <param name="id">The EthnicityId of the Ethnicity to return.</param>
        /// <returns>
        /// 405 - Method Not Allowed
        /// </returns>
		[ResponseType(typeof(EthnicityDTO))]
		public IHttpActionResult Get(int id)
        {
            return StatusCode(HttpStatusCode.MethodNotAllowed);
        }

        // PUT api/Ethnicity/5
        /// <summary>
        /// Method Not Allowed
        /// </summary>
        /// <param name="id">The EthnicityId of the Ethnicity to save.</param>
        /// <param name="Ethnicity">The DTO of the edited Ethnicity</param>
        /// <returns>
        /// 405 - Method Not Allowed
        /// </returns>
        public IHttpActionResult Put(int id, EthnicityDTO Ethnicity)
        {
            return StatusCode(HttpStatusCode.MethodNotAllowed);
        }

        // POST api/Ethnicity/
        /// <summary>
        /// Method Not Allowed
        /// </summary>
        /// <param name="Ethnicity">The new Ethnicity</param>
        /// <returns>
        /// 405 - Method Not Allowed
        /// </returns>
		[ResponseType(typeof(EthnicityDTO))]
		public IHttpActionResult Post(EthnicityDTO Ethnicity)
        {
            return StatusCode(HttpStatusCode.MethodNotAllowed);
        }

        // DELETE api/Ethnicity/5
        /// <summary>
        /// Method Not Allowed
        /// </summary>
        /// <param name="id">The EthnicityId of the Ethnicity to delete.</param>
        /// <returns>
        /// 405 - Method Not Allowed
        /// </returns>
		[ResponseType(typeof(EthnicityDTO))]
		public IHttpActionResult Delete(int id)
        {
            return StatusCode(HttpStatusCode.MethodNotAllowed);
        }
    }
}
