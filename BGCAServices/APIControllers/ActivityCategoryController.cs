﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using BGCAServices.Data;
using BGCAServices.Models;
using BGCAServices.Classes;

namespace BGCAServices.APIControllers
{
    /// <summary>
    /// ActivityCategory Controller - Basic CRUD for ActivityCategory
    /// </summary>
    public class ActivityCategoryController : ApiController
    {
	
		private BGCAEntities db = new BGCAEntities();

        // GET api/ActivityCategory/
        /// <summary>
        /// An iQueryable ActivityCategory lookup
        /// </summary>
        /// <returns>
        /// 200 - Success + A list of ActivityCategory
        /// 401 - Not Authorized 
        /// 500 - Internal Server Error + Exception
        /// </returns>
        public IQueryable<ActivityCategoryDTO> Get()
        {
            return db.ActivityCategories.Select(o => new ActivityCategoryDTO
            {
                id = o.id, 
                categoryid = o.categoryid,
                activityid = o.activityid
            });
        }

        // GET api/ActivityCategory/5
        /// <summary>
        /// Retrieve a single ActivityCategory from the database.
        /// </summary>
        /// <param name="id">The ActivityCategoryId of the ActivityCategory to return.</param>
        /// <returns>
        /// 200 - Success + The requested ActivityCategory.
        /// 401 - Not Authorized 
        /// 404 - Not Found + Reason
        /// </returns>
		[ResponseType(typeof(ActivityCategoryDTO))]
		public IHttpActionResult Get(int id)
        {
            ActivityCategoryDTO activitycategory = Get().FirstOrDefault<ActivityCategoryDTO>(o => o.id == id);
            if (activitycategory == null)
            {
                return this.NotFound("ActivityCategory not found.");
            }

            return Ok(activitycategory);
        }

        // PUT api/ActivityCategory/5
        /// <summary>
        /// Method Not Allowed.
        /// </summary>
        /// <param name="id">The ActivityCategoryId of the ActivityCategory to save.</param>
        /// <param name="activitycategoryDTO">The DTO of the edited ActivityCategory</param>
        /// <returns>
        /// 405 - Method Not Allowed
        /// </returns>
        public IHttpActionResult Put(int id, ActivityCategoryDTO activitycategoryDTO)
        {
            return StatusCode(HttpStatusCode.MethodNotAllowed);
        }

        // POST api/ActivityCategory/
        /// <summary>
        /// A new ActivityCategory to be added.
        /// </summary>
        /// <param name="activitycategoryDTO">The new ActivityCategory</param>
        /// <returns>
        /// 201 - Created + The new ActivityCategory
        /// 400 - Bad Request + (Invalid Model State)
        /// 401 - Not Authorized 
        /// 404 - Not Found + Reason
        /// 500 - Internal Server Error + Exception
        /// </returns>
		[ResponseType(typeof(ActivityCategoryDTO))]
		public IHttpActionResult Post(ActivityCategoryDTO activitycategoryDTO)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            try
            {
				ActivityCategory activitycategory = new ActivityCategory();
                activitycategory.activityid = activitycategoryDTO.activityid;
                activitycategory.categoryid = activitycategoryDTO.categoryid;
                
                db.ActivityCategories.Add(activitycategory);
                db.SaveChanges();

                activitycategoryDTO.id = activitycategory.id;

                return CreatedAtRoute("DefaultApi", new { id = activitycategory.id }, activitycategoryDTO);
            }
            catch (APIException ex)
            {
                if (ex.code == 404)
                    return this.NotFound(ex.message);
                else
                    return this.InternalServerError(ex);
            }
            catch (Exception e)
            {
                return this.InternalServerError(e);
            }
        }

        // DELETE api/ActivityCategory/5
        /// <summary>
        /// Delete a ActivityCategory from the database.
        /// </summary>
        /// <param name="id">The ActivityCategoryId of the ActivityCategory to delete.</param>
        /// <returns>
		/// 200 - Success + The deleted ActivityCategory 
		/// 401 - Not Authorized 
        /// 405 - Method Not Allowed
		/// 500 - Internal Server Error + the Exception
        /// </returns>
		[ResponseType(typeof(ActivityCategoryDTO))]
		public IHttpActionResult Delete(int id)
        {
            try {
				// For Objects which cannot be deleted:
				// return StatusCode(HttpStatusCode.MethodNotAllowed);  // Update Return Codes
				ActivityCategory activitycategory = db.ActivityCategories.Find(id);
				if (activitycategory == null)
				{
					return this.NotFound("ActivityCategory not found.");
				}

                ActivityCategoryDTO returnDTO = Get().FirstOrDefault<ActivityCategoryDTO>(o => o.id == id);
                db.ActivityCategories.Remove(activitycategory);
                db.SaveChanges();

                return Ok(returnDTO);
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }
    }
}
