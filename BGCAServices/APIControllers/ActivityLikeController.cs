﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using BGCAServices.Data;
using BGCAServices.Models;
using BGCAServices.Classes;

namespace BGCAServices.APIControllers
{
    /// <summary>
    /// ActivityLike Controller - Basic CRUD for ActivityLike
    /// </summary>
    public class ActivityLikeController : ApiController
    {
	
		private BGCAEntities db = new BGCAEntities();

        // GET api/ActivityLike/
        /// <summary>
        /// An iQueryable ActivityLike lookup
        /// </summary>
        /// <returns>
        /// 200 - Success + A list of ActivityLike
        /// 401 - Not Authorized 
        /// 500 - Internal Server Error + Exception
        /// </returns>
        public IQueryable<ActivityLikeDTO> Get()
        {
            return db.ActivityLikes.Select(o => new ActivityLikeDTO
            {
                id = o.id, 
                activityid = o.activityid,
                userid = o.userid
            });
        }

        // GET api/ActivityLike/5
        /// <summary>
        /// Retrieve a single ActivityLike from the database.
        /// </summary>
        /// <param name="id">The ActivityLikeId of the ActivityLike to return.</param>
        /// <returns>
        /// 200 - Success + The requested ActivityLike.
        /// 401 - Not Authorized 
        /// 404 - Not Found + Reason
        /// </returns>
		[ResponseType(typeof(ActivityLikeDTO))]
		public IHttpActionResult Get(int id)
        {
            ActivityLikeDTO activitylike = Get().FirstOrDefault<ActivityLikeDTO>(o => o.id == id);
            if (activitylike == null)
            {
                return this.NotFound("ActivityLike not found.");
            }

            return Ok(activitylike);
        }

        // PUT api/ActivityLike/5
        /// <summary>
        /// Method Not Allowed.
        /// </summary>
        /// <param name="id">The ActivityLikeId of the ActivityLike to save.</param>
        /// <param name="activitylikeDTO">The DTO of the edited ActivityLike</param>
        /// <returns>
        /// 204 - No Content
        /// 400 - Bad Request + (Invalid Model State)
        /// 401 - Not Authorized 
        /// 404 - Not Found + Reason
        /// 500 - Internal Server Error + Exception
        /// </returns>
        public IHttpActionResult Put(int id, ActivityLikeDTO activitylikeDTO)
        {
            return StatusCode(HttpStatusCode.MethodNotAllowed);
        }

        // POST api/ActivityLike/
        /// <summary>
        /// A new ActivityLike to be added.
        /// </summary>
        /// <param name="activitylikeDTO">The new ActivityLike</param>
        /// <returns>
        /// 201 - Created + The new ActivityLike
        /// 400 - Bad Request + (Invalid Model State)
        /// 401 - Not Authorized 
        /// 404 - Not Found + Reason
        /// 500 - Internal Server Error + Exception
        /// </returns>
		[ResponseType(typeof(ActivityLikeDTO))]
		public IHttpActionResult Post(ActivityLikeDTO activitylikeDTO)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            try
            {
                ActivityLike activitylike = new ActivityLike();
                activitylike.activityid = activitylikeDTO.activityid;
                activitylike.userid = activitylikeDTO.userid;

                db.ActivityLikes.Add(activitylike);
                
                Activity activity = db.Activities.FirstOrDefault(o => o.id == activitylikeDTO.activityid);
                if (activity == null)
                    throw new APIException("Activity Not Found.", 404);
                activity.likes = activity.likes + 1;
                db.Entry(activity).State = EntityState.Modified;

                db.SaveChanges();

                activitylikeDTO.id = activitylike.id;

                return CreatedAtRoute("DefaultApi", new { id = activitylike.id }, activitylikeDTO);
            }
            catch (APIException ex)
            {
                if (ex.code == 404)
                    return this.NotFound(ex.message);
                else
                    return this.InternalServerError(ex);
            }
            catch (Exception e)
            {
                return this.InternalServerError(e);
            }
        }

        // DELETE api/ActivityLike/5
        /// <summary>
        /// Delete a ActivityLike from the database.
        /// </summary>
        /// <param name="id">The ActivityLikeId of the ActivityLike to delete.</param>
        /// <returns>
		/// 200 - Success + The deleted ActivityLike 
		/// 401 - Not Authorized 
        /// 405 - Method Not Allowed
		/// 500 - Internal Server Error + the Exception
        /// </returns>
		[ResponseType(typeof(ActivityLikeDTO))]
		public IHttpActionResult Delete(int id)
        {
            try {
				// For Objects which cannot be deleted:
				// return StatusCode(HttpStatusCode.MethodNotAllowed);  // Update Return Codes
				ActivityLike activitylike = db.ActivityLikes.Find(id);
				if (activitylike == null)
				{
					return this.NotFound("ActivityLike not found.");
				}

                ActivityLikeDTO returnDTO = Get().FirstOrDefault<ActivityLikeDTO>(o => o.id == id);
                db.ActivityLikes.Remove(activitylike);

                Activity activity = db.Activities.FirstOrDefault(o => o.id == activitylike.activityid);
                if (activity == null)
                    throw new APIException("Activity Not Found.", 404);
                activity.likes = activity.likes - 1;
                db.Entry(activity).State = EntityState.Modified;

                db.SaveChanges();

                return Ok(returnDTO);
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }
    }
}
