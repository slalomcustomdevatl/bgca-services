﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using BGCAServices.Data;
using BGCAServices.Models;
using BGCAServices.Classes;

namespace BGCAServices.APIControllers
{
    /// <summary>
    /// Category Controller - Basic CRUD for Category
    /// </summary>
    public class CategoryController : ApiController
    {
	
		private BGCAEntities db = new BGCAEntities();

        // GET api/Category/
        /// <summary>
        /// An iQueryable Category lookup
        /// </summary>
        /// <returns>
        /// 200 - Success + A list of Category
        /// 401 - Not Authorized 
        /// 500 - Internal Server Error + Exception
        /// </returns>
        public IQueryable<CategoryDTO> Get()
        {
            return db.Categories.Select(o => new CategoryDTO
            {
                id = o.id, 
                description = o.description
            });
        }

        // GET api/Category/5
        /// <summary>
        /// Retrieve a single Category from the database.
        /// </summary>
        /// <param name="id">The CategoryId of the Category to return.</param>
        /// <returns>
        /// 200 - Success + The requested Category.
        /// 401 - Not Authorized 
        /// 404 - Not Found + Reason
        /// </returns>
		[ResponseType(typeof(CategoryDTO))]
		public IHttpActionResult Get(int id)
        {
            CategoryDTO category = Get().FirstOrDefault<CategoryDTO>(o => o.id == id);
            if (category == null)
            {
                return this.NotFound("Category not found.");
            }

            return Ok(category);
        }

        // PUT api/Category/5
        /// <summary>
        /// Save changes to a single Category to the database.
        /// </summary>
        /// <param name="id">The CategoryId of the Category to save.</param>
        /// <param name="categoryDTO">The DTO of the edited Category</param>
        /// <returns>
        /// 204 - No Content
        /// 400 - Bad Request + (Invalid Model State)
        /// 401 - Not Authorized 
        /// 404 - Not Found + Reason
        /// 500 - Internal Server Error + Exception
        /// </returns>
        public IHttpActionResult Put(int id, CategoryDTO categoryDTO)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != categoryDTO.id)
            {
                return BadRequest();
            }

            try
            {
				Category category = db.Categories.FirstOrDefault(o => o.id == id);
				if (category == null)
                    throw new APIException("Category not found.", 404);
                category.description = categoryDTO.description;

                db.Entry(category).State = EntityState.Modified;
                db.SaveChanges();
                return StatusCode(HttpStatusCode.NoContent);
            }
            catch (APIException ex)
            {
                if (ex.code == 404)
                    return this.NotFound(ex.message);
                else
                    return this.InternalServerError(ex);
            }
            catch (Exception e)
            {
                return this.InternalServerError(e);
            }
        }

        // POST api/Category/
        /// <summary>
        /// A new Category to be added.
        /// </summary>
        /// <param name="categoryDTO">The new Category</param>
        /// <returns>
        /// 201 - Created + The new Category
        /// 400 - Bad Request + (Invalid Model State)
        /// 401 - Not Authorized 
        /// 404 - Not Found + Reason
        /// 500 - Internal Server Error + Exception
        /// </returns>
		[ResponseType(typeof(CategoryDTO))]
		public IHttpActionResult Post(CategoryDTO categoryDTO)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            try
            {
				Category category = new Category();
                category.description = categoryDTO.description;
                db.Categories.Add(category);
                
                db.SaveChanges();

                categoryDTO.id = category.id;

                return CreatedAtRoute("DefaultApi", new { id = category.id }, categoryDTO);
            }
            catch (APIException ex)
            {
                if (ex.code == 404)
                    return this.NotFound(ex.message);
                else
                    return this.InternalServerError(ex);
            }
            catch (Exception e)
            {
                return this.InternalServerError(e);
            }
        }

        // DELETE api/Category/5
        /// <summary>
        /// Delete a Category from the database.
        /// </summary>
        /// <param name="id">The CategoryId of the Category to delete.</param>
        /// <returns>
		/// 200 - Success + The deleted Category 
		/// 401 - Not Authorized 
		/// 500 - Internal Server Error + the Exception
        /// </returns>
		[ResponseType(typeof(CategoryDTO))]
		public IHttpActionResult Delete(int id)
        {
            try {
				Category category = db.Categories.Find(id);
				if (category == null)
				{
					return this.NotFound("Category not found.");
				}

                CategoryDTO returnDTO = Get().FirstOrDefault<CategoryDTO>(o => o.id == id);
                db.Categories.Remove(category);
                db.SaveChanges();

                return Ok(returnDTO);
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }
    }
}
