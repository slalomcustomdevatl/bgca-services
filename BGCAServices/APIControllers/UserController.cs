﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using BGCAServices.Classes;
using BGCAServices.Data;
using BGCAServices.Models;

namespace BGCAServices.Controllers
{
    /// <summary>
    /// User Controller - Basic User CRUD
    /// </summary>
    public class UserController : ApiController
    {
        private BGCAEntities db = new BGCAEntities();

        // GET api/User
        /// <summary>
        /// Method Not Allowed
        /// </summary>
        /// <returns>
        /// 404 - Method Not AllowedS
        /// </returns>
        public IHttpActionResult GetActivities()
        {
            return StatusCode(HttpStatusCode.MethodNotAllowed);
        }

        // GET api/User/5
        /// <summary>
        /// Retrieve a single User from the database.
        /// </summary>
        /// <param name="id">The UserId of the User to return.</param>
        /// <returns>
        /// 200 - Success + The requested User.
        /// 401 - Not Authorized 
        /// 404 - Not Found + Reason
        /// </returns>
        [ResponseType(typeof(UserDTO))]
        public IHttpActionResult GetUser(string id)
        {
            User user = db.Users.FirstOrDefault(s => s.email == id);
            
            if (user == null)
            {
                return NotFound();
            }
            UserDTO dto = GetDTO(user);
            return Ok(dto);
        }

        // PUT api/User/5
        /// <summary>
        /// Method Not Allowed.
        /// </summary>
        /// <param name="id">The UserId of the User to save.</param>
        /// <param name="inventoryModel">The model of the edited User</param>
        /// <returns>
        /// 405 - Method Not Allowed
        /// </returns>
        public IHttpActionResult Put(int id, UserDTO userModel)
        {
            return StatusCode(HttpStatusCode.MethodNotAllowed);
        }

        // POST api/User/1/Story/1
        /// <summary>
        /// Add a favorite Story to a User.
        /// </summary>
        /// <param name="userid">The user</param>
        /// <param name="storyid">The story</param>
        /// <returns>
        /// 201 - Created + The new User
        /// 401 - Not Authorized 
        /// 404 - Not Found + Reason
        /// 500 - Internal Server Error + Exception
        /// </returns>
        [ResponseType(typeof(UserDTO))]
        [Route("api/User/{userid:int}/Story/{storyid:int}", Name = "FavoriteUserStoryRoute")]
        public IHttpActionResult PostUserStory(int userid, int storyid)
        {
            try
            {
                User user = db.Users.FirstOrDefault(u => u.id == userid);
                if (user == null)
                    return this.NotFound("User not found.");
                Story story = db.Stories.FirstOrDefault(a => a.id == storyid);
                if (story == null)
                    return this.NotFound("Story not found.");
                user.Stories.Add(story);
                db.Entry(user).State = EntityState.Modified;
                db.SaveChanges();
                UserDTO dto = GetDTO(user);
                return CreatedAtRoute("FavoriteUserStoryRoute", new { userid = user.id, storyid = story.id }, dto);
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        // POST api/User/1/Activity/1
        /// <summary>
        /// Add a favorite Activity to a User.
        /// </summary>
        /// <param name="userid">The User</param>
        /// <param name="activityid">The Activity</param>
        /// <returns>
        /// 201 - Created + The new User
        /// 401 - Not Authorized 
        /// 404 - Not Found + Reason
        /// 500 - Internal Server Error + Exception
        /// </returns>
        [ResponseType(typeof(UserDTO))]
        [Route("api/User/{userid:int}/Activity/{activityid:int}", Name = "FavoriteUserActivityRoute")]
        public IHttpActionResult PostUserActivity(int userid, int activityid)
        {
            try
            {
                User user = db.Users.FirstOrDefault(u => u.id == userid);
                if (user == null)
                    return this.NotFound("User not found.");
                Activity activity = db.Activities.FirstOrDefault(a => a.id == activityid);
                if (activity == null)
                    return this.NotFound("Activity not found.");
                user.Activities.Add(activity);
                db.Entry(user).State = EntityState.Modified;
                db.SaveChanges();
                UserDTO dto = GetDTO(user);
                return CreatedAtRoute("FavoriteUserActivityRoute", new { userid = user.id, activityid = activity.id }, dto);
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        // POST api/User
        /// <summary>
        /// A new User to be added.
        /// </summary>
        /// <param name="activity">The new User</param>
        /// <returns>
        /// 201 - Created + The new User
        /// 400 - Bad Request + (Invalid Model State)
        /// 401 - Not Authorized 
        /// 404 - Not Found + Reason
        /// 500 - Internal Server Error + Exception
        /// </returns>
        [ResponseType(typeof(UserDTO))]
        public IHttpActionResult PostUser(UserDTO user)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            //check if user exists
            if (db.Users.Where(w => w.email == user.email).Select(s => s).Count() == 0)
            {
                User newuser = new User();
                newuser.name = user.name;
                newuser.email = user.email;

                db.Users.Add(newuser);
                db.SaveChanges();
                
                user.id = newuser.id;
                return CreatedAtRoute("DefaultApi", new { id = user.id }, user);
            }
            else
                return BadRequest();
        }

        // DELETE api/User/5
        /// <summary>
        /// Method Not Allowed.
        /// </summary>
        /// <param name="id">The UserId of the User to delete.</param>
        /// <returns>
        /// 405 - Method Not Allowed
        /// </returns>
        [ResponseType(typeof(UserDTO))]
        public IHttpActionResult Delete(int id)
        {
            return StatusCode(HttpStatusCode.MethodNotAllowed);
        }

        // DELETE api/User/1/Story/1
        /// <summary>
        /// Unfavorite a Story for a User.
        /// </summary>
        /// <param name="userid">The User</param>
        /// <param name="storyid">The Story</param>
        /// <returns>
        /// 200 - Success + The updated User 
        /// 401 - Not Authorized 
        /// 500 - Internal Server Error + the Exception
        /// </returns>
        [ResponseType(typeof(UserDTO))]
        [Route("api/User/{userid:int}/Story/{storyid:int}", Name = "UnfavoriteUserStoryRoute")]
        public IHttpActionResult DeleteUserStory(int userid, int storyid)
        {
            try
            {
                User user = db.Users.FirstOrDefault(u => u.id == userid);
                if (user == null)
                    return this.NotFound("User not found.");
                Story story = db.Stories.FirstOrDefault(a => a.id == storyid);
                if (story == null)
                    return this.NotFound("Story not found.");
                user.Stories.Remove(story);
                db.Entry(user).State = EntityState.Modified;
                db.SaveChanges();
                UserDTO dto = GetDTO(user);
                return Ok(dto);
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }

        // DELETE api/User/1/Activity/1
        /// <summary>
        /// Unfavorite a Activity for a User.
        /// </summary>
        /// <param name="userid">The User</param>
        /// <param name="activityid">The Activity</param>
        /// <returns>
        /// 200 - Success + The updated User 
        /// 401 - Not Authorized 
        /// 500 - Internal Server Error + the Exception
        /// </returns>
        [ResponseType(typeof(UserDTO))]
        [Route("api/User/{userid:int}/Activity/{activityid:int}", Name = "UnfavoriteUserActivityRoute")]
        public IHttpActionResult DeleteUserActivity(int userid, int activityid)
        {
            try
            {
                User user = db.Users.FirstOrDefault(u => u.id == userid);
                if (user == null)
                    return this.NotFound("User not found.");
                Activity activity = db.Activities.FirstOrDefault(a => a.id == activityid);
                if (activity == null)
                    return this.NotFound("Activity not found.");
                user.Activities.Remove(activity);
                db.Entry(user).State = EntityState.Modified;
                db.SaveChanges();
                UserDTO dto = GetDTO(user);
                return Ok(dto);
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool UserExists(int id)
        {
            return db.Users.Count(e => e.id == id) > 0;
        }

        /// <summary>
        /// Helper method for casting User --> UserDTO
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        private UserDTO GetDTO(User user)
        {
            UserDTO dto = new UserDTO();
            dto.id = user.id;
            dto.name = user.name;
            dto.email = user.email;
            dto.activities = user.Activities.Select(o => o.id).ToList<int>();
            dto.stories = user.Stories.Select(o => o.id).ToList<int>();
            return dto;
        }
    }
}